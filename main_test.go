package main

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"encoding/json"
	"net/http"
	"net/http/httptest"
    "os"
	"testing"
	"github.com/stretchr/testify/assert"
    "log"
)

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	exitCode := m.Run()

	os.Exit(exitCode)
}

func router() *gin.Engine {
	router := gin.Default()

	publicRoutes := router.Group("/")

	publicRoutes.GET("/books", GetBooks)
	publicRoutes.POST("/books",CreateBook)
	publicRoutes.GET("/books/:id", BookById)
	publicRoutes.PATCH("/checkout",CheckoutBook)
	publicRoutes.PATCH("/return",ReturnBook)

	return router
}
func makeRequest(method, url string,body interface{}) *httptest.ResponseRecorder {
	requestBody, _ := json.Marshal(body)
	request, _ := http.NewRequest(method, url, bytes.NewBuffer(requestBody))
	writer := httptest.NewRecorder()
	router().ServeHTTP(writer, request)
	return writer
}

func TestGetAllEntries(t *testing.T) {
	writer := makeRequest("GET", "/books",nil)
	assert.Equal(t, http.StatusOK, writer.Code)
	log.Println("Executing TestGetAllEntries in done")
}

func TestNonExistingGetBookByID(t *testing.T){
	writer := makeRequest("GET", "/books/24",nil)
	assert.Equal(t, http.StatusNotFound, writer.Code)
	log.Println("Executing TestNonExistingGetBookByID in done")
}
func TestExistingGetBookByID(t *testing.T){
	writer := makeRequest("GET", "/books/2",nil)
	assert.Equal(t, http.StatusOK, writer.Code)
	log.Println("Executing TestExistingGetBookByID in done")
}
func TestCreateBook(t *testing.T){
	body := map[string]interface{}{
		"id": "4",
		"title": "Hamlet",
		"author": "William Shakespeare",
		"quantity": 0,
	}
	writer := makeRequest("POST", "/books",body)
	assert.Equal(t, http.StatusCreated, writer.Code)
	log.Println("Executing TestCreateBook in done")
}
func TestCheckoutBook(t *testing.T){
	writer := makeRequest("PATCH", "/checkout?id=1",nil)
	assert.Equal(t, http.StatusOK, writer.Code)
	log.Println("Executing TestCheckoutBook in done")
}
func TestCheckoutBookZeroQuantity(t *testing.T){
	writer := makeRequest("PATCH", "/checkout?id=4",nil)
	assert.Equal(t, http.StatusBadRequest, writer.Code)
	log.Println("Executing TestCheckoutBookZeroQuantity in done")
}
func TestReturnBook(t *testing.T){
	writer := makeRequest("PATCH", "/return?id=1",nil)
	assert.Equal(t, http.StatusOK, writer.Code)
	log.Println("Executing TestReturnBook in done")
}
func TestReturnBookNotFound(t *testing.T){
	writer := makeRequest("PATCH", "/return?id=5",nil)
	assert.Equal(t, http.StatusNotFound, writer.Code)
	log.Println("Executing TestReturnBookNotFound in done")
}